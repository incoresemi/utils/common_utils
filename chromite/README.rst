Chromite tools setup
======================

The bash script installs all the tools required for Chromite build

.. code-block:: bash

  ./tools_setup.sh 
  
   tools_setup 0.1.0
   ================================================
  
   Tools dir : /home/lavanya/check_tools/common_utils/chromite/chromite_tools...
  
   Usage: sudo JOBS=<n> ./tools_setup.sh <command> <install_path>
   
   Command to install the following tools. JOBS option when given
   is passed on to make command during installation.
  
   Eg. sudo JOBS=`nproc` ./tools_setup.sh all $PWD 
  
   Available commands: 
   sudo ./tools_setup.sh help 			 Displays help
   sudo ./tools_setup.sh bsc <path> 		 Install Bluespec Compiler
   sudo ./tools_setup.sh verilator <path>		 Install Verilator
   sudo ./tools_setup.sh riscv_tools <path>		 Install RISC-V GNU Toolchain
   sudo ./tools_setup.sh openocd <path>		 Install RISC-V OpenOCD
   sudo ./tools_setup.sh dtc <path>		 Install DTC 1.4.7
   sudo ./tools_setup.sh python_setup <path>	 Install pyenv and Python 3.7.0
   sudo ./tools_setup.sh all <path>		 Installs  all the above tools
   sudo ./tools_setup.sh clean_builds <path> []	 Removes all build dirs
                                     		 Usage: sudo ./tools_setup.sh clean_builds <path> all
   sudo ./tools_setup.sh clean_all <path> []	 Removes all src and build dirs
                                     		 Usage: sudo ./tools_setup.sh clean_all <path> all
   sudo ./tools_setup.sh export_path <path> 	 Sets up path variable
 
General Usage
===============

.. code-block:: bash
  
  sudo JOBS=100 ./tools_setup.sh all $PWD
  source tools_setup.sh export_path $PWD
