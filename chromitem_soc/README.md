# ChromiteM SoC tools setup

To buid, simulate and port the ChromiteM SoC on the FPGA you will need to install the following tools. NOTE: The following have been tested on Ubuntu 20.04. 

We suggest you create a separate directory for the chromitem-soc related tools. We shall call this directory `chromitem_tools`

```bash=
mkdir ~/chromitem_tools
```

In the following guide `num_jobs` is the number of parallel jobs that can be used to build the tool.

## Install Miniterm

We will be using this for monioring the serial console from the fpga.

```bash=
sudo apt-get install python3-serial
```

## Bluespec Compiler

Bluespec is a High Level Hardware description language. If the following steps don't work then please refer to the original installation readme available [here](https://github.com/B-Lang-org/bsc/blob/main/INSTALL.md)

```bash=
sudo apt-get update
sudo apt install -y ghc libghc-regex-compat-dev \
    libghc-syb-dev iverilog libghc-old-time-dev \
    libfontconfig1-dev libx11-dev libghc-split-dev \
    libxft-dev flex bison libxft-dev tcl-dev tk-dev \
    libfontconfig1-dev libx11-dev gperf itcl3-dev \
    itk3-dev autoconf git libcanberra-gtk-module \
    libcanberra-gtk3-module gtkwave build-essential \
    pkg-config
```

If you would like to build the docs (Reference and User guide for BSV) then texlive also needs to be installed:

```bash=
sudo apt-get install \
    texlive-latex-base \
    texlive-latex-recommended \
    texlive-latex-extra \
    texlive-font-utils \
    texlive-fonts-extra
```

Clone the repository and intall bsc:

```bash=
mkdir ~/chromitem_tools/mybsc/
cd ~/
git clone --recursive https://github.com/B-Lang-org/bsc
cd bsc
git checkout 2021.07
make PREFIX=~/chromitem_tools/mybsc/ install-src
```

If you want to build the docs:
```bash=
make PREFIX=~/chromitem_tools/mybsc/ install-doc
```

Update your `$PATH`. Bash users can do the following:

```bash=
echo 'export PATH=$PATH:~/chromitem_tools/mybsc//bin' >> ~/.bashrc 
```

Open a new terminal and check the following:

```bash=
bsc -help
```

## Verilator Verilog Simulator

[Verilator](https://www.veripool.org/verilator/) is a fast open-source Verilog/SystemVerilog simulator. For a detailed installation guide, refer this [link](https://verilator.org/guide/latest/install.html). **DO NOT INSTALL FROM PACKAGE MANAGER!**. We need a specific version of Verilator(4.106), hence better to go with git installation. 
 
**PLEASE CHANGE `<user>` below your username.**

```bash=
mkdir ~/chromitem_tools/myverilator/
sudo apt-get -y install perl python3 make g++ 
sudo apt-get -y install ccache libgoogle-perftools-dev \
    numactl perl-doc autoconf flex bison 
    
## Following is for Ubuntu systems only (ignore is raises error)
sudo apt-get -y install libfl2 libfl-dev zlib1g zlib1g-dev
cd ~/
git clone https://github.com/verilator/verilator 
unset VERILATOR_ROOT  # For bash shell 
cd verilator 
git checkout v4.106 
autoconf 
./configure --prefix=/home/<user>/chromitem_tools/myverilator/
make –j<num_jobs>
make install 
echo 'export PATH=$PATH:~/chromitem_tools/myverilator//bin' >> ~/.bashrc
```

## Spike - Instruction Set Simulator

While this is useful for porting applications, we also need the elf2hex utility embedded in this tool for simulations

```bash=
mkdir ~/chromitem_tools/myspike/
cd ~/
git clone https://github.com/riscv-software-src/riscv-isa-sim.git
cd riscv-isa-sim
git checkout 1afbe648b8eb36bd9caf641d8d5365ffb3069557
mkdir build
cd build
../configure --prefix=/home/<user>/chromitem_tools/myspike/ --enable-commitlog
make ij <num_jobs>
make install
echo 'export PATH=$PATH:~/chromitem_tools/myspike//bin' >> ~/.bashrc
```

## GNU Toolchain

```bash=
sudo apt-get install autoconf automake autotools-dev curl \
    python3 libmpc-dev libmpfr-dev libgmp-dev gawk \
    build-essential bison flex texinfo gperf libtool \
    patchutils bc zlib1g-dev libexpat-dev
```
If you are using RHEL/CentOS/ArchLinux or OS X please refer to the installation guide [here](https://github.com/riscv-software-src/riscv-gnu-toolchain) for downloading pre-requisites.

Installing the gnu-toolchain

```bash=
mkdir ~/chromitem_tools/myriscv/
cd ~/
git clone https://github.com/riscv/riscv-gnu-toolchain
cd riscv-gnu-toolchain
git checkout 1a36b5dc44d71ab6a583db5f4f0062c2a4ad963b
./configure --prefix=/home/<user>/chromitem_tools/myriscv/ --with-arch=rv64imac --with-abi=lp64 --with-cmodel=medany
make -j <num_jobs>
echo 'export PATH=$PATH:~/chromitem_tools/myriscv/bin' >> ~/.bashrc
```

## Vivado

Xilinx Vivado is a propreitary tool required to generate bitstream and program your FPGA.

Create the installation directory first

```bash=
mkdir ~/chromitem_tools/myvivado
```

1. [Create an account](https://www.xilinx.com/registration/create-account.html) on Xilinx  and download the Vivado Design Suite [2019.2 Linux Unified Installer](https://www.xilinx.com/member/forms/download/xef.html?filename=Xilinx_Unified_2019.2_1106_2127_Lin64.bin) . 
2. Run the Xilinx unified installer

  ```bash=
    chmod +x Xilinx_Unified_2019.2_1106_2127_Lin64.bin
    ./Xilinx_Unified_2019.2_1106_2127_Lin64.bin
  ```
3. Install Vivado Design Suite HLx with the default settings that are present in the installer. Make sure to install them under the path: `~/chromitem_soc/vivado/`
4. Install Xilinx cable drivers. Assuming, you set the install path in the previous step as /tools/Xilinx, run the following commands:

  ```bash=

    cd ~/chromitem_tools/myvivado/Vivado/2019.2/data/xicom/cable_drivers/lin64/install_script/install_drivers
    sudo ./install_drivers
  ```

5. Add the Vivado binary path to your `$PATH`

  ```bash=
    echo 'export PATH=$PATH:~/chromitem_tools/myvivado/Vivado/2019.2/bin' >> ~/.bashrc
  ```
6. Add Vivado board files for Digilent FPGA boards:

  ```bash=
    git clone https://github.com/Digilent/vivado-boards.git
    cp -r vivado-boards/new/board_files/*  ~/chromitem_tools/myvivado/Vivado/2019.2/data/boards/board_files/*
  ```
  
## RISC-V OPENOCD

This will be required to connect the GNU debugger on your host to the FPGA for debugging purposes.

Create the installation directory

```bash=
mkdir ~/chromitem_tools/myopenocd/
```

Clone and install the InCore variant of the tool:

```bash=
sudo apt-get install --no-install-recommends -y libusb-1.0
git clone https://gitlab.com/incoresemi/software/riscv-openocd.git
cd riscv-openocd/
git checkout incorespi-addition
./bootstrap
./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=/home/<user>/chromitem_tools/myopenocd/
make -j <num_jobs>
make install
echo 'export PATH=$PATH:~/chromitem_tools/myopenocd/bin' >> ~/.bashrc
```

## Miniconda 

[Miniconda](https://docs.conda.io/en/latest/miniconda.html) is a lightweight installer for conda (a python package management system). It will help us in creating, managing multiple python environments. Using python environments, we can isolate issues, dependency problems from affecting other environments or the system.

```bash=  
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/chromitem_tools/miniconda.sh 
# yes to accept licence, enter to default location, yes to conda init 
bash ~/chromitem_tools/miniconda.sh 
~/miniconda3/bin/conda init bash 
echo 'export PATH=~/chromitem_tools/miniconda3/bin:$PATH' >>~/.bashrc 
```

Make sure to open a new terminal for changes in bashrc to be reflected and do the following:

```bash=
conda config --set auto_activate_base false 
```

### Conda environment: 

Here we are creating an environment specically for ChromiteM-Soc. So we are naming the environment “chromitem_soc”. [This](https://www.youtube.com/watch?v=1VVCd0eSkYc) video explains more about conda environments. 

 
```bash=
conda create --name "chromitem_soc" python=3.7 --yes 
conda activate chromitem_soc 
```

Now install the python dependencies required by ChromiteM-SoC

```bash=
cd chromitem_soc # whereever you have cloned the repo
pip install -r requirements.txt
```


 




  
 


