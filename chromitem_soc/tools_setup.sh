# tools_setup

### Constants
VERSION=0.1.0
SETUP_PREFIX="."

if [ -z $2 ]; then
  SETUP_PREFIX="$PWD/chromite_tools"
  printf "\n tools_setup ${VERSION}\n"
  printf " ================================================\n"
  printf "\n Tools dir : ${SETUP_PREFIX}...\n"
elif [ -d "$2" ]; then
  SETUP_PREFIX="$2/chromite_tools"
  printf "\n tools_setup ${VERSION}\n"
  printf " ================================================\n"
  printf "\n Tools dir : ${SETUP_PREFIX}...\n"
else
  echo "ERROR: Path ${SETUP_PREFIX} not found. Can not continue."
  exit 1
fi




## Usage help function
usage () {
    printf "\n"
    printf " Usage: sudo JOBS=<n> ./tools_setup.sh <command> <install_path>\n"
    printf " \n"
    printf " Command to install the following tools. JOBS option when given\n"
    printf " is passed on to make command during installation.\n\n"
    printf " Eg. sudo JOBS=\`nproc\` ./tools_setup.sh all \$PWD \n\n" 
    printf " Available commands: \n"
    printf " sudo ./tools_setup.sh help \t\t\t Displays help\n"
    printf " sudo ./tools_setup.sh bsc <path> \t\t Install Bluespec Compiler\n"
    printf " sudo ./tools_setup.sh verilator <path>\t\t Install Verilator\n"
    printf " sudo ./tools_setup.sh riscv_tools <path>\t\t Install RISC-V GNU Toolchain\n"
    printf " sudo ./tools_setup.sh openocd <path>\t\t Install RISC-V OpenOCD\n"
    printf " sudo ./tools_setup.sh mod_spike <path>\t\t Install Modified Spike\n"
    printf " sudo ./tools_setup.sh dtc <path>\t\t Install DTC 1.4.7\n"
    printf " sudo ./tools_setup.sh python_setup <path>\t Install pyenv and Python 3.7.0\n"
    printf " sudo ./tools_setup.sh all <path>\t\t Installs  all the above tools\n"
    printf " sudo ./tools_setup.sh clean_builds <path> []\t Removes all build dirs\n"
    printf "                                   \t\t Usage: sudo ./tools_setup.sh clean_builds <path> all\n"
    printf " sudo ./tools_setup.sh clean_all <path> []\t Removes all src and build dirs\n"
    printf "                                   \t\t Usage: sudo ./tools_setup.sh clean_all <path> all\n"
    printf " sudo ./tools_setup.sh export_path <path> \t Sets up path variable\n"
    printf "\n"
    exit 1
}

common () {
  sudo apt update
  sudo apt-get install --no-install-recommends -y git make cmake python3-serial wget
  mkdir -p ${SETUP_PREFIX}
}

clone_repo () {
  local dirname=$(extract_name "$1")
  if [ -d $SETUP_PREFIX/$dirname ]; then
    echo "Updating Repo: " $1 
    cd $SETUP_PREFIX/$dirname
    git pull origin $2
    git checkout $2
    if [ -d "build" ]; then
      rm -rf build
    fi
  else
    echo "Cloning Repo: " $1} 
    git clone $1 $SETUP_PREFIX/$dirname
    (cd $SETUP_PREFIX/$dirname; git checkout $2;)
    if [ $dirname = "riscv-gnu-toolchain" ]; then
      (cd $SETUP_PREFIX/$dirname; git submodule init)
      (cd $SETUP_PREFIX/$dirname; sed -i '/qemu/d' .gitmodules)
      (cd $SETUP_PREFIX/$dirname; sed -i '/qemu/d' .git/config)
      (cd $SETUP_PREFIX/$dirname; git submodule update --recursive)
    else
      (cd $SETUP_PREFIX/$dirname; git submodule update --init --recursive)
    fi
    cd $SETUP_PREFIX/$dirname
  fi
}

clean_builds () {
  
  cd $SETUP_PREFIX

  if [ -z $2 ]; then
    echo "\n Warning: Command to clean all builds or use tool names to clean specific tool build"
    echo "\n Eg. $ sudo ./tools_setup.sh clean_builds <tool_path> [all|bsc|riscv_tools|dtc .. ]\n"
    exit 1
  fi

  if [ -d $2/build ]; then
    echo "\n1  Cleaning build $SETUP_PREFIX/$2/build"
    rm -rf "$2/build"
  else 
    for i in $(ls -d */); do echo " Deleting ${i}build"; rm -rf ${i}build; done
  fi

}

clean_all () {
  
  cd $SETUP_PREFIX

  if [ -z $2 ]; then
    echo "\n Warning: Command to clean all builds or use tool names to clean specific tool build"
    echo "\n Eg. $ sudo ./tools_setup.sh clean_all <tool_path> [all|bsc|riscv_tools|dtc .. ]\n"
    exit 1
  fi

  if [ -d $2/build ]; then
    echo "\n1  Cleaning build $SETUP_PREFIX/$2"
    rm -rf "$2"
  else 
    for i in $(ls -d */); do echo " Deleting ${i}"; rm -rf ${i}; done
  fi

}

export_path () {
  
  cd $SETUP_PREFIX
  
  for i in $(ls -d */); 
  do
    echo " Path setup ${i}"
    export PATH=$PWD/${i}build/bin:$PATH
    export LD_LIBRARY_PATH=$PWD/${i}build/lib:$LD_LIBRARY_PATH
  done
  
  if [ -d "/home/$USER/.pyenv" ]; then
    export PATH="/home/$USER/.pyenv/bin:$PATH"
  fi
  
  if [ -d "$SETUP_PREFIX/verilator" ]; then
    export VERILATOR_INC=$SETUP_PREFIX/verilator
  fi
  if [ -d "$SETUP_PREFIX/mod-spike/riscv-isa-sim/build" ]; then
    export PATH=$PWD/mod-spike/riscv-isa-sim/build/bin:$PATH
    export LD_LIBRARY_PATH=$PWD/mod-spike/riscv-isa-sim/build/lib:$LD_LIBRARY_PATH
  fi
}

check_version () {
  local version=$($1 --version | rev | cut -d' ' -f1 | rev)
  local minimum=$(echo -e "$version\n$2" | sort -V | head -n1)
  if [ $2 == $version ] && return 1 || [ $version == $minimum ]; then
    echo "Please update $1 to $2 or above"
    exit 1
  fi
}

## Die function
err() { echo "$*" 1>&2 ;}

## Extract repo directory name
extract_name () {
  echo "$1"| rev | cut -d'/' -f1 | rev
}

install_riscv_gnu () {

  clone_repo "https://github.com/riscv/riscv-gnu-toolchain" "1a36b5dc44d71ab6a583db5f4f0062c2a4ad963b"

  printf "\n RISC-V GNU Toolchain Installation\n"
  printf "================================================\n"
  mkdir -p build
  export RISCV=$PWD/build
  cd build
  ../configure --prefix=$RISCV --with-arch=rv64imac --with-abi=lp64 --with-cmodel=medany
  make -j ${JOBS}
}

install_bsc () {

  clone_repo "https://github.com/B-Lang-org/bsc" "2021.07"
  
  printf "\n Bluespec Compiler Installation\n"
  printf "================================================\n"
  sudo apt-get install --no-install-recommends -y ghc libghc-regex-compat-dev libghc-syb-dev \
    libghc-old-time-dev libghc-split-dev tcl-dev autoconf gperf flex bison \
    texinfo gawk libtool g++ 

  mkdir -p build
  make -j ${JOBS} all PREFIX=$PWD/build GHCJOBS=${JOBS}

}

install_verilator () {

  
  clone_repo "https://git.veripool.org/git/verilator" "stable"
  
  printf "\n Verilator Installation\n"
  printf "================================================\n"
  sudo apt-get install --no-install-recommends -y git make autoconf g++ flex bison libfl2 libfl-dev  # Ubuntu only (ignore if gives error)

  unset VERILATOR_ROOT  # For bash
  autoconf        # Create ./configure script
  mkdir -p build
  ./configure --prefix $PWD/build
  make -j ${JOBS}
  make install
}

install_mod_spike () {

  clone_repo "https://gitlab.com/shaktiproject/tools/mod-spike" "bump-to-latest"

  printf "\n Mod Spike Installation\n"
  printf "================================================\n"
  git clone https://github.com/riscv/riscv-isa-sim.git
  cd riscv-isa-sim
  git checkout 6d15c93fd75db322981fe58ea1db13035e0f7add
  git apply ../shakti.patch
  mkdir -p build
  export RISCV=$PWD/build
  cd build
  ../configure --prefix=$RISCV
  make -j ${JOBS}
  make install

}

install_openocd () {

  clone_repo "https://github.com/riscv/riscv-openocd" "2ea18ef7f6998e6df2a4c5870858990cc64f71a2"
  
  printf "\n OpenOCD Installation\n"
  printf "================================================\n"
  mkdir -p build
  export RISCV=$PWD/build
  sudo apt-get install --no-install-recommends -y libusb-1.0
  ./bootstrap
  ./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV
  make -j ${JOBS}
  make install

}

install_dtc () {
  
  cd ${SETUP_PREFIX}
  wget https://git.kernel.org/pub/scm/utils/dtc/dtc.git/snapshot/dtc-1.4.7.tar.gz
  tar -xvzf dtc-1.4.7.tar.gz
  cd dtc-1.4.7/
  make NO_PYTHON=1 PREFIX=/usr/
  make install NO_PYTHON=1 PREFIX=/usr/
}

install_python_setup () {
  
  cd ${SETUP_PREFIX}
  sudo apt-get install --no-install-recommends -y make build-essential libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libsqlite3-dev wget curl \
    llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

  curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

  #export PATH="/home/$USER/.pyenv/bin:$PATH"
  #eval "$(pyenv init -)"
  #eval "$(pyenv virtualenv-init -)"

  #pyenv install 3.7.0
  #pyenv virtualenv 3.7.0 myenv

}
### Main Script

## Check if no command line args passed, print help and exit
if [ "$#" -eq 0 ]; then
    usage
fi

case $1 in
    help)
        usage
        ;;
    bsc)
        common
        install_bsc
        ;;  
    verilator)
        common
        install_verilator
        ;;  
    mod_spike)
        common
        install_mod_spike
        ;;  
    openocd)
        common
        install_openocd
        ;;  
    dtc)
        common
        install_dtc
        ;;  
    riscv_tools)
        common
        install_riscv_gnu
        ;;
    python_setup)
        common
        install_python_setup
        ;;
    all)
        common
        install_bsc
        install_verilator
        install_mod_spike
        install_openocd
        install_dtc
        install_riscv_gnu
        install_python_setup
        ;;
    clean_builds)
        clean_builds "$2" "$3"
        ;;
    clean_all)
        clean_all "$2" "$3"
        ;;
    export_path)
        export_path
        ;;
    *)
        printf "\n ERROR: Unrecognized tool option \n"
        ;;


esac



