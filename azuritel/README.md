## AzuriteL tools setup

To build, simulate and port the Azuritel you will need to install the following tools. NOTE: The following have been tested on Ubuntu 22.04.

We suggest creating a separate directory for the Azuritel related tools. Let the directory be named `azuritel`

```bash=
mkdir ~/azuritel
```

In the following guide `num_jobs` is the number of parallel jobs that can be used to build the tool.

## Installing DTC

For Ubuntu/Debian systems:

```bash=
sudo apt-get install device-tree-compiler
```

For Fedora/CentOS/RHEL systems:

```bash=
sudo dnf install dtc
```

## Installing pyenv and python3

Python 3.8 is required for the tools used. We can use pyenv to manage multiple python versions and virtual environments. Install pyenv and python 3.8 with the following steps:

For Ubuntu/Debian systems:

```bash=
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
```

For Fedora/CentOS/RHEL systems:

```bash=
sudo yum install gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite \
sqlite-devel openssl-devel xz xz-devel libffi-devel
```

Installing pyenv:

```bash=
curl = https://pyenv.run | bash
```

Add the following to the bashrc:

```bash=
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```

Run the following to restart shell and check if pyenv works:

```bash=
exec "$SHELL"
```

You can install different versions of Python using pyenv, so lets install python 3.8.0 using pyenv:

```bash=
pyenv install -v 3.8.0
```

If you are having issues downloading using the above step, check this [link](https://www.python.org/ftp/python/3.7.2/) and use grep to install the required version.

Type the following to check the versions installed:

```bash=
pyenv versions
```

You can change Python versions using the following:

```bash=
pyenv global <version>
python -V
```

You can also use the local command in place of global to set it for the particular virtual environment.

## Installing Bluespec Compiler

Bluespec is a high level hardware description langauge. If the following steps don't work then please refer to the original installation README available [here](https://github.com/B-Lang-org/bsc/blob/main/INSTALL.md)

```bash=
sudo apt-get update
sudo apt install -y ghc libghc-regex-compat-dev \
     libghc-syb-dev iverilog libghc-old-time-dev \
     libfontconfig1-dev libx11-dev libghc-split-dev \
     libxft-dev flex bison tcl-dev tk-dev \
     gperf itcl3-dev \
     itk3-dev autoconf git libcanberra-gtk-module \
     libcanberra-gtk3-module gtkwave build-essential \
     pkg-config
```

If you would like to build the docs(Reference and User guide for BSV) then texlive also needs to be installed.

```bash=
sudo apt-get install \
    texlive-latex-base \
    texlive-latex-recommended \
    texlive-latex-extra \
    texlive-font-utils \
    texlive-fonts-extra
```

Clone the repository and install BSC:

```bash=
mkdir ~/azuritel/mybsc/
git clone --recursive https://github.com/B-Lang-org/bsc
cd bsc
git checkout 2021.07
make PREFIX=~/azuritel/mybsc/ install-src
```

If you want to build the docs:

```bash=
make PREFIX=~/azuritel/mybsc/ install-doc
```

Update the `$PATH`. Bash users can do the following:

```bash=
echo 'export PATH=$PATH:~/azuritel/mybsc//bin' >> ~/.bashrc 
```

Zsh users can do the following:

```bash=
echo 'export PATH=$PATH:~/azuritel/mybsc//bin' >> ~/.zshrc 
```

Open a new terminal and check the following:

```bash=
bsc -help
```

## Verilator - Verilog Simulator

[Verilator](https://www.veripool.org/verilator/) is a fast open-source Verilog/SystemVerilog simulator. For a detailed installation guide, refer this [link](https://verilator.org/guide/latest/install.html). **DO NOT INSTALL FROM PACKAGE MANAGER!**. We need a specific version of Verilator that is tweaked by Incore, hence better to go with git installation.

**For the below installation, PLEASE CHANGE THE `<user>` TO YOUR USERNAME**

```bash=
mkdir ~/azuritel/myverilator/
sudo apt-get -y install perl make g++ 
sudo apt-get -y install ccache libgoogle-perftools-dev \
    numactl perl-doc autoconf flex bison 
    
## Following is for Ubuntu systems only (ignore is raises error)
sudo apt-get -y install libfl2 libfl-dev zlib1g zlib1g-dev

git clone https://github.com/incoresemi/verilator.git
unset VERILATOR_ROOT  # For bash shell 
cd verilator
git checkout incore-dev
autoconf 
./configure --prefix=/home/<user>/azuritel/myverilator/
make -j<num_jobs>
make install 
echo 'export PATH=$PATH:~/azuritel/myverilator//bin' >> ~/.bashrc
```

## Spike - Instruction Set Simulator

Spike is a RISC-V ISA simulator that supports a large variety of instruction sets. Similar to the verilator installation, we are required to install a specific version of Spike following the steps below:

```bash=
mkdir ~/azuritel/myspike/

git clone https://github.com/incoresemi/riscv-isa-sim.git
cd riscv-isa-sim
git checkout incoresemi
mkdir build
cd build
../configure --prefix=/home/<user>/azuritel/myspike/ --enable-commitlog
make -j<num_jobs>
make install
echo 'export PATH=$PATH:~/azuritel/myspike//bin' >> ~/.bashrc
```

## GNU Toolchain

Note that a specific version of Python is required for the tools used so make sure to follow the pyenv installation before proceeding.

```bash=
sudo apt-get install autoconf automake autotools-dev curl \
    libmpc-dev libmpfr-dev libgmp-dev gawk \
    build-essential bison flex texinfo gperf libtool \
    patchutils bc zlib1g-dev libexpat-dev
```
If you are using RHEL/CentOS/ArchLinux or OS X please refer to the installation guide [here](https://github.com/riscv-software-src/riscv-gnu-toolchain) for downloading pre-requisites.

Installing the gnu-toolchain

```bash=
mkdir ~/azuritel/myriscv/
git clone https://github.com/riscv/riscv-gnu-toolchain
cd riscv-gnu-toolchain
git checkout 2022.09.21
./configure --prefix=/software/azuritel/ --with-arch=rv32imc_zicsr_zifencei_zba_zbb_zbc_zbs --with-abi=ilp32 --with-cmodel=medany
make -j<num_jobs>
echo 'export PATH=$PATH:~/azuritel/myriscv/bin' >> ~/.bashrc
```

## OpenOCD

```bash=
mkdir ~/azuritel/myocd/
git clone https://gitlab.com/incoresemi/software/riscv-openocd
cd riscv-openocd
git checkout incorespi-addition
./bootstrap
./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=/azuritel/myocd/ --enable-ftdi-oscan1
make -j<num_jobs>
make install
```

## ABC Sequential Logic Synthesis

```bash=
mkdir ~/azuritel/abc/
git clone https://github.com/berkeley-abc/abc
cd abc
make
ln -s $(pwd)/abc ~/azuritel/abc
```
